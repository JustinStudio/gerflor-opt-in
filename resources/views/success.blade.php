<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>OPT-IN to stay connected to the world of flooring</title>
        <meta name="description" content="The law is changing, so to keep up to date with Gerflor, fill in your details below and click register. If you do nothing, from 25 May 2018 you will lose your access to the world of Gerflor.  We would love to have you on board. ">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}" media="all">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}" media="all">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
            function onSubmit(token) {
                document.getElementById("subscribeform").submit();
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="https://www.gerflor.co.uk/"><img src="{{ URL::asset('images/logo.png') }}" alt="Gerflor" class="img-responsive logo"/></a>
                </div>
                <div class="col-md-10">
                    <ul class="nav">
                        <li><a href="https://www.gerflor.co.uk/product-ranges.html">PRODUCTS</a></li>
                        <li><a href="https://www.gerflor.co.uk/sectors.html">SECTORS</a></li>
                        <li><a href="https://www.gerflor.co.uk/case-studies/case-studies.html">CASE STUDIES</a></li>
                        <li><a href="https://www.gerflor.co.uk/services-pro.html">SERVICES</a></li>
                        <li><a href="https://www.gerflor.co.uk/services-pro/contact-pro.html">CONTACT</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('partials.notifications')
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 thanks">
                    <h1>Thank you</h1>
                    <p>Your opt in choice has been made.</p>
                    <a href="https://www.gerflor.co.uk/" class="btn btn-new">Visit Gerflor Website</a>
                </div>
            </div>
        </div>
        <div class="container-fluid footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Gerflor Group creates, manufactures, and markets innovative, decorative and sustainable vinyl flooring solutions and wall finishes. We develop specific flooring and wall solutions to meet every indoor market application need : housing, healthcare, education, sport, retail, industry, offices, hospitality and transport vehicles. Gerflor Group gathers several world-renowned product brand names, such as Taraflex®, Mipolam®, Tarabus®, Connor Sports®, Sportcourt® and Gradus®. 
                        </p>
                        
                        <ul class="social-networks-list">
                            <li>
                                <a href="https://www.facebook.com/pages/Gerflor-UK/581319941995336" class="social-icon-container">
                                    <img src="{{ URL::asset('images/f.png') }}" alt="Facebook"/>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/Gerfloruk" class="social-icon-container">
                                    <img src="{{ URL::asset('images/t.png') }}" alt="Twitter"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://pinterest.com/gerfloruk/" class="social-icon-container">
                                    <img src="{{ URL::asset('images/p.png') }}" alt="Pinterest"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.youtube.com/channel/UC0irIoOLQO4NaT_MvYJpJuA" class="social-icon-container">
                                    <img src="{{ URL::asset('images/y.png') }}" alt="Youtube"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.linkedin.com/company/gerflor" class="social-icon-container">
                                    <img src="{{ URL::asset('images/in.png') }}" alt="Linked In"/>
                                </a>
                            </li>
                            <li>                                                                                                                                                                 
                                <a href="https://www.instagram.com/gerfloruk/" class="social-icon-container">
                                    <img src="{{ URL::asset('images/insta.png') }}" alt="Instagram"/>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-6">
                        <a href="https://www.gerflor.co.uk/"><img class="img-responsive footerLogo" src="{{ URL::asset('images/logo-white.png') }}" alt="Gerflor" /></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="copyright">Copyright © 2018 Gerflor. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>