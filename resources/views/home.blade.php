<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>OPT-IN to stay connected to the world of flooring</title>
        <meta name="description" content="The law is changing, so to keep up to date with Gerflor, fill in your details below and click register. If you do nothing, from 25 May 2018 you will lose your access to the world of Gerflor.  We would love to have you on board. ">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/app.css') }}" media="all">
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/style.css') }}" media="all">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script>
            function onSubmit(token) {
                document.getElementById("subscribeform").submit();
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <a href="https://www.gerflor.co.uk/"><img src="{{ URL::asset('images/logo.png') }}" alt="Gerflor" class="img-responsive logo"/></a>
                </div>
                <div class="col-md-10">
                    <ul class="nav">
                        <li><a href="https://www.gerflor.co.uk/product-ranges.html">PRODUCTS</a></li>
                        <li><a href="https://www.gerflor.co.uk/sectors.html">SECTORS</a></li>
                        <li><a href="https://www.gerflor.co.uk/case-studies/case-studies.html">CASE STUDIES</a></li>
                        <li><a href="https://www.gerflor.co.uk/services-pro.html">SERVICES</a></li>
                        <li><a href="https://www.gerflor.co.uk/services-pro/contact-pro.html">CONTACT</a></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    @include('partials.notifications')
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1>Opt-in now and get on board to stay connected to the world of flooring</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <img src="{{ URL::asset('images/gdpr-boat-image.jpg') }}" alt="Gerflor" class="img-responsive"/>
                    <p class="reference">This picture has been created using references from the Gerflor collections</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>So far you have been in the know on all things about Gerflor. From seeing the latest innovations, designs and collections to practical technical and services support. The future looks more exciting than ever before.</p>
                    <p>The law is changing, so to keep up to date with Gerflor, fill in your details below and click register. If you do nothing, from <strong>25th May 2018</strong> you will lose your access to the world of Gerflor. We would love to have you on board. </p>
                </div>
            </div>
            <?php
                $firstname="";
                $lastname="";
                $email="";
                $telephone="";
                if(isset($_GET['firstname'])){
                    $firstname = $_GET['firstname'];
                }
                if(isset($_GET['lastname'])){
                    $lastname = $_GET['lastname'];
                }
                if(isset($_GET['email'])){
                    $email = $_GET['email'];
                }
                if(isset($_GET['telephone'])){
                    $telephone = $_GET['telephone'];
                }
            ?>
            <div class="row">   
                <div class="col-sm-offset-2 col-sm-8">
                    <form id="subscribeform" action="/subscribe" method="POST">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <input type="firstname" name="firstname" class="form-control" value="<?=$firstname?>" placeholder="First Name*">
                            </div>
                            <div class="col-md-6">
                                <input type="lastname" name="lastname" class="form-control" value="<?=$lastname?>" placeholder="Last Name*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="email" name="email" class="form-control" value="<?=$email?>" placeholder="Email*">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="telephone" name="telephone" class="form-control" value="<?=$telephone?>" placeholder="Telephone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" name="choice" value="emailonly" class="regular-checkbox">
                            </div>
                            <div class="col-xs-11">
                                <label class="checkbox" for="emailonly"> Yes please, I'd like to hear about products and services in the future by email only</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" name="choice" value="emailphone" class="regular-checkbox">
                            </div>
                            <div class="col-xs-11">
                                <label class="checkbox" for="emailphone"> Yes please, I'd like to hear about products and services in the future by email and telephone</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-1">
                                <input type="radio" name="choice" value="nocontact" class="regular-checkbox">
                            </div>
                            <div class="col-xs-11">
                                <label class="checkbox" for="nocontect"> No thanks, I don't want to hear about your products and services in the future</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>By completing this contact form we will hold your information on file and may contact you in the future with information that maybe of relevance to you.</p>
                                <p>We will not pass your information on to any 3rd parties. This contact form will not submit unless you select one of the boxes.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 centerise">
                                <br/>
                                <button class="btn btn-new g-recaptcha" data-sitekey="6Lfj51UUAAAAAPecbp7aobgx0VF-N4SSmJ9y9BpT" data-callback="onSubmit">REGISTER</button>
                                <!--<button class="btn btn-new g-recaptcha" data-callback="onSubmit">REGISTER</button>-->
                                <br/><br/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="container-fluid footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            Gerflor Group creates, manufactures, and markets innovative, decorative and sustainable vinyl flooring solutions and wall finishes. We develop specific flooring and wall solutions to meet every indoor market application need : housing, healthcare, education, sport, retail, industry, offices, hospitality and transport vehicles. Gerflor Group gathers several world-renowned product brand names, such as Taraflex®, Mipolam®, Tarabus®, Connor Sports®, Sportcourt® and Gradus®. 
                        </p>
                        
                        <ul class="social-networks-list">
                            <li>
                                <a href="https://www.facebook.com/pages/Gerflor-UK/581319941995336" class="social-icon-container">
                                    <img src="{{ URL::asset('images/f.png') }}" alt="Facebook"/>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/Gerfloruk" class="social-icon-container">
                                    <img src="{{ URL::asset('images/t.png') }}" alt="Twitter"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://pinterest.com/gerfloruk/" class="social-icon-container">
                                    <img src="{{ URL::asset('images/p.png') }}" alt="Pinterest"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.youtube.com/channel/UC0irIoOLQO4NaT_MvYJpJuA" class="social-icon-container">
                                    <img src="{{ URL::asset('images/y.png') }}" alt="Youtube"/>
                                </a>
                            </li>
                            <li>
                                <a href="http://www.linkedin.com/company/gerflor" class="social-icon-container">
                                    <img src="{{ URL::asset('images/in.png') }}" alt="Linked In"/>
                                </a>
                            </li>
                            <li>                                                                                                                                                                 
                                <a href="https://www.instagram.com/gerfloruk/" class="social-icon-container">
                                    <img src="{{ URL::asset('images/insta.png') }}" alt="Instagram"/>
                                </a>
                            </li>
                        </ul>

                    </div>
                    <div class="col-md-6">
                        <a href="https://www.gerflor.co.uk/"><img class="img-responsive footerLogo" src="{{ URL::asset('images/logo-white.png') }}" alt="Gerflor" /></a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="copyright">Copyright © 2018 Gerflor. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>