First Name: {{ $firstname }}<br/>
Last Name: {{ $lastname }}<br/>
Email: {{ $email }}<br/>
Telephone: {{ $telephone }}<br/>
@if($emailonly == 1)
 Email contact only.
@endif
@if($emailphone == 1)
 Email and telephone contact.
@endif
@if($nocontact == 1)
 No contact allowed.
@endif