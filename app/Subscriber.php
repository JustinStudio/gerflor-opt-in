<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    protected $fillable = [
    'firstname',
    'lastname',
    'email',
    'telephone',
    'emailonly',
    'emailphone',
    'nocontact'
  ];
}
