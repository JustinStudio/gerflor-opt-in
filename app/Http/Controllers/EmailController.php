<?php



namespace App\Http\Controllers;



use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;


use Validator;
use Mail;


class EmailController extends Controller
{

  public function store() {
      $data = Input::all();

      $messages = [

        'firstname.required' => 'Your first name is required.',

		'lastname.required' => 'Your last name is required.',

        'email.required|email' => 'Your email address is required.',

        'choice.required' => 'You must select a contact option.'

    ];

    $validator = Validator::make(

		  $data, array(

        'firstname' => 'required',

        'lastname' => 'required',

        'email' => 'required|email',

        'choice' => 'required'

			),

			$messages

    );

    if ($validator->fails()) {

		  $messages = $validator->messages();

			return redirect()->back()->withInput()->withErrors($messages); 

    } else {
        $data['emailonly'] = 0;
        $data['emailphone'] = 0;
        $data['nocontact'] = 0;
        if($data['choice'] == "emailonly"){
            $data['emailonly'] = 1;
        }
        if($data['choice'] == "emailphone"){
            $data['emailphone'] = 1;
        }
        if($data['choice'] == "nocontact"){
            $data['nocontact'] = 1;
        }
        \App\Subscriber::create($data);

            Mail::send('emails.subscribe', $data, function ($message) use ($data) {

		    	$message->to('jake@thestudio4.co.uk')->cc('justin@thestudio4.co.uk')->from('no-reply@gerflornews.co.uk')->subject("New Gerflor Subscription");

		    });

        return view('success');
    }

    }
}